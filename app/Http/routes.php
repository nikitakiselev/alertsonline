<?php

Route::group(['middleware' => 'web'], function () {

    Route::get('/', ['as' => 'front', 'uses' => 'SiteController@index']);
    Route::get('about', ['as' => 'about', 'uses' => 'SiteController@about']);

    Route::auth();
    Route::get('confirmation', 'Auth\AuthController@confirmation');
    Route::get('confirmation/success', 'Auth\AuthController@confirmationSuccess');
    Route::get('confirm/{hash}', 'Auth\AuthController@confirm');
    Route::get('blocked', 'Auth\AuthController@blocked');
    Route::get('activate', 'Auth\AuthController@activate');

    Route::get('widgets/alert-box/{hash}', [
        'as' => 'widget.donate_alertbox',
        'uses' => 'WidgetController@donateAlertBox'
    ]);

    Route::get('donate/{username}', [
        'as' => 'donate',
        'uses' => 'DonationController@donate'
    ]);

    Route::post('donate/{username}', 'DonationController@postDonate');

    Route::group(['prefix' => 'dashboard', 'middleware' => 'auth', 'namespace' => 'Dashboard'], function() {

        Route::get('/', [
            'as' => 'dashboard',
            'uses' => 'DashboardController@index'
        ]);

        Route::get('donations', [
            'as' => 'donations',
            'uses' => 'DonationController@index',
        ]);

        Route::get('test-donation', [
            'as' => 'donation.test',
            'uses' => 'DonationController@test'
        ]);

        Route::group(['prefix' => 'widgets', 'as' => 'widget.'], function() {

            Route::get('donation', [
                'as' => 'donation',
                'uses' => 'WidgetController@donateSettings'
            ]);

            Route::post('donation', 'WidgetController@saveDonateSettings');
        });

        /*
         * Настройки
         */
        Route::group(['prefix' => 'settings'], function() {

            Route::get('/', [
                'as' => 'dashboard.settings',
                'uses' => 'SettingsController@getAccount',
            ]);

            Route::post('/', 'SettingsController@postAccount');

            Route::get('donate', [
                'as' => 'dashboard.settings.donate',
                'uses' => 'SettingsController@getDonate'
            ]);

            Route::post('donate', 'SettingsController@postDonate');

            Route::get('withdrawal', [
                'as' => 'dashboard.settings.withdrawal',
                'uses' => 'SettingsController@getWithdrawal'
            ]);

            Route::post('withdrawal', 'SettingsController@postWithdrawal');
        });
    });

    Route::group(['prefix' => 'payment'], function() {
        /**
         * По этому адресу unitpay будет уведомлять о любом изменении в статусе платежа.
         */
        Route::get('proccess', 'PaymentController@proccess');
    });
});