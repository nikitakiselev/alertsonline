<?php

namespace App\Http\Middleware;

use Closure;

class ActivatedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->active) {
            return $next($request);
        }

        // activeted but not active et. blocked
        if (\Auth::user()->activated()) {
            \Auth::logout();
            return redirect()->to('/blocked');
        }

        // user not activated his email
        \Auth::logout();
        return redirect()->to('/activate');
    }
}
