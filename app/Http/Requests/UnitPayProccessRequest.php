<?php

namespace App\Http\Requests;

use App\DigitalHammer\UnitPay\UnitPay;
use App\Http\Requests\Request;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exception\HttpResponseException;

class UnitPayProccessRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \UnitPay::isAllowedIp($this->ip());
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->get('user_id'));
        $minAmountRule = $user ? 'min:' . $user->settings->get('donate_minimum_amount') : '';

        return [
            'method'    => 'required|in:check,pay,error',
            'unitpayId' => 'required', // unique
            'account'   => 'required',
            'user_id'   => 'required|exists:users,id',
            'sum'       => 'required|numeric|' . $minAmountRule,
            'username' =>  'required|max:255',
            'speaker'   => 'required',
            'emotion'   => 'required',
            'message'   => 'required|max:255'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator $validator
     *
     * @return mixed
     *
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $unitpay = $this->container->make(UnitPay::class);

        throw new HttpResponseException($unitpay->getErrorHandlerResponse($this->formatErrors($validator)));
    }

    /**
     * Format the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $message = $validator->getMessageBag();

        return implode(', ', $message->all());
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return mixed
     *
     * @throws \\Illuminate\Http\Exception\HttpResponseExceptio
     */
    protected function failedAuthorization()
    {
        throw new HttpResponseException(app('unitpay')->getErrorHandlerResponse($this->forbiddenResponse()));
    }

    /**
     * Get the response for a forbidden operation.
     *
     * @return \Illuminate\Http\Response
     */
    public function forbiddenResponse()
    {
        return "Access denied for ip " . $this->ip();
    }
}
