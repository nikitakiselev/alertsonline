<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WidgetController extends Controller
{
    /**
     * Donate alert widget settings
     */
    public function donateSettings()
    {
        return view('dashboard.widget.donate', compact('settings'));
    }

    /**
     * @param SaveDonateSettingsRequest $request
     */
    public function saveDonateSettings(SaveDonateSettingsRequest $request)
    {
        auth()->user()->settings->update($request->all())->save();
        flash()->success('Настройки успешно изменены.');

        return redirect()->back();
    }
}
