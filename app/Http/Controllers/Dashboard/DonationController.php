<?php

namespace App\Http\Controllers\Dashboard;

use App\Donation;
use App\Events\UserWasDonated;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DonationController extends Controller
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * @var Donation
     */
    private $donation;


    /**
     * DonationsController constructor.
     *
     * @param Donation $donation
     */
    public function __construct(Guard $auth, Donation $donation)
    {
        $this->auth = $auth;
        $this->donation = $donation;
    }


    /**
     * Get user's donations page
     */
    public function index()
    {
        $donations = $this->auth->user()->donations()->latest()->paginate(20);
        $user = $this->auth->user();

        return view('dashboard.donations.index', compact('donations', 'user'));
    }

    /**
     * Send test donation alert
     */
    public function test()
    {
        $donation = new Donation();
        $donation->user_id = $this->auth->user()->id;
        $donation->username = 'Вачик Магасян';
        $donation->amount = 200;
        $donation->message = 'Тестовое сообщение';
        $donation->speaker = 'zahar';
        $donation->emotion = 'good';
        $donation->robot = false;

        event(new UserWasDonated($donation));
    }
}
