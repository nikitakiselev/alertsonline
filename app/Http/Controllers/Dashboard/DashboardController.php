<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Dashboard index
     */
    public function index()
    {
        $lastDonations = auth()->user()
            ->donations()
            ->latest()
            ->take(5)
            ->get();

        return view('dashboard.index', compact('lastDonations'));
    }
}
