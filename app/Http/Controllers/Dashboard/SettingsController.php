<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * SettingsController constructor.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->user = $auth->user();
    }

    /**
     * Get account settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccount()
    {
        return view('dashboard.settings.account');
    }

    /**
     * Get donate settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDonate()
    {
        return view('dashboard.settings.donate');
    }

    /**
     * Get Withdrawal settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getWithdrawal()
    {
        return view('dashboard.settings.withdrawal');
    }

    /**
     * Update account settings
     *
     * @param Request $request
     */
    public function postAccount(Request $request)
    {
        $this->user->settings->update($request->all())->save();

        flash()->success('Настройки сохранены');

        return redirect()->back();
    }

    /**
     * Save donate settings
     *
     * @param Request $request
     */
    public function postDonate(Request $request)
    {
        $this->validate($request, [
            'donate_text' => 'max:300',
            'donate_bg'   => 'image'
        ]);

        $user = $this->auth->user();

        $user->settings->update($request->all())->save();

        flash()->success('Настройки сохранены');

        return redirect()->back();
    }

    /**
     * Save withdrawal settings
     */
    public function postWithdrawal(Request $request)
    {
        $this->validate($request, [
            'withdraw_webmoney_purse' => 'required|size:13'
        ]);

        $user = $this->auth->user();
        $user->settings->update($request->all())->save();

        flash()->success('Настройки сохранены');

        return redirect()->back();
    }
}
