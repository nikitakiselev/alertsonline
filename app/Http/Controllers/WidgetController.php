<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveDonateSettingsRequest;

class WidgetController extends Controller
{
    /**
     * Donate alert box
     *
     * @param $hash
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function donateAlertBox($hash)
    {
        $user = User::where('widget_token', $hash)->first();

        if (!$user) {
            abort(404);
        }

        return view('widgets.donate-alert', compact('user', 'hash'));
    }
}
