<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Events\UserWasRegister;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'blocked', 'activate']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:50|lat_alpha_dash|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'active' => false,
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());
        // Auth::guard($this->getGuard())->login($user);

        $hash = hash_hmac('sha256', str_random(40), config('app.key'));
        $user->confirmation_code = $hash;
        $user->save();

        event(new UserWasRegister($user));

        return redirect('/confirmation');
    }

    /**
     * Окончание регистрации. Сообщение о необходимости подтвердить email
     */
    public function confirmation()
    {
        return view('auth.confirmation.confirmation');
    }

    /**
     * Подтверждение регистрации
     *
     * @param $hash
     */
    public function confirm($hash)
    {
        $user = User::where('confirmation_code', $hash)->first();

        if (!$user) {
            abort(404);
        }

        $user->confirmation_code = '';
        $user->active = true;
        $user->save();

        // login user
        Auth::guard($this->getGuard())->login($user);

        flash()->success('Ваша электронная почта подтверждена, аккаунт успешно активирован. Добро пожаловать на сайт.');

        return redirect($this->redirectPath());
    }

    /**
     * Blocked user page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blocked()
    {
        return view('auth.blocked');
    }

    /**
     * Activate message page
     */
    public function activate()
    {
        return view('auth.activate');
    }
}
