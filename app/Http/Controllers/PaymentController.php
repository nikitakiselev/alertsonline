<?php

namespace App\Http\Controllers;

use App\Donation;
use App\Events\UserWasDonated;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Logging\Log;
use App\DigitalHammer\UnitPay\UnitPay;
use App\Http\Requests\UnitPayProccessRequest;

class PaymentController extends Controller
{
    /**
     * @var Illuminate\Contracts\Logging\Log
     */
    private $log;

    /**
     * @var UnitPay
     */
    private $unitPay;

    /**
     * PaymentController constructor.
     *
     * @param Log $log
     */
    public function __construct(Log $log, UnitPay $unitPay)
    {
        $this->log = $log;
        $this->unitPay = $unitPay;
    }


    /**
     * Обработка ответов unitpay
     *
     * @param UnitPayProccessRequest $request
     */
    public function proccess(UnitPayProccessRequest $request)
    {
        $this->log->info('Unitpay: обработка платежа', $request->all());

        switch ($request->get('method'))
        {
            case 'check':
                return $this->checkResponse($request);

            case 'pay':
                return $this->payResponse($request);

            case 'error':
                return $this->errorResponse($request);
        }

        return $this->unitPay->getErrorHandlerResponse('Unknown method ' . $request->get('method'));
    }

    /**
     * Проверка возможности оказания услуги абоненту.
     *
     * Вы должны подтвердить готовность системы (проверить корректность суммы платежа, существование счета в БД и т.д.).
     *
     * @param UnitPayProccessRequest $request
     */
    private function checkResponse(UnitPayProccessRequest $request)
    {
        return $this->unitPay->getSuccessHandlerResponse('Запрос успешно обработан');
    }

    /**
     * Уведомление об успешном списании, Вы должны оказать услугу абоненту.
     *
     * При любой ошибке на данном этапе (например не доступна БД) платеж получает статус «незавершен»,
     * после устранения проблем Вы можете повторно провести платеж в статистике.
     *
     * @param UnitPayProccessRequest $request
     */
    private function payResponse(UnitPayProccessRequest $request)
    {
        $user = User::find($request->get('user_id'));

        $input = $request->except('sum');
        $input['amount'] = $request->get('sum');

        $donation = $user->donations()->create($input);

        event(new UserWasDonated($donation));

        return $this->unitPay->getSuccessHandlerResponse('Запрос успешно обработан');
    }

    /**
     * Ошибка платежа на любом из этапов.
     *
     * Если ошибка вызвана пустым/ошибочным ответом сервера партнера, то запрос отправлен не будет.
     * Следует учесть, что данный статус не конечный и возможны ситуации когда после запроса
     * ERROR может последовать запрос PAY.
     *
     * @param UnitPayProccessRequest $request
     */
    private function errorResponse(UnitPayProccessRequest $request)
    {
        $this->log->error('UnitPay: Ошибка платежа на одном из этапов', $request->all());
    }
}
