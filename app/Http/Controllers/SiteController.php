<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function index()
    {
        return view('layouts.app-landing');
    }

    public function about()
    {
        return view('layouts.app');
    }
}
