<?php

namespace App\Http\Controllers;

use App\DigitalHammer\UnitPay\UnitPay;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Events\UserWasDonated;
use App\Http\Controllers\Controller;

class DonationController extends Controller
{
    /**
     * Donation page
     *
     * @param $username
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function donate($username)
    {
        $user = User::where('name', $username)->first();

        if ( ! $user) {
            abort(404);
        }

        return view('pages.donate', compact('user', 'username'));
    }

    /**
     * @param Request $request
     * @param UnitPay $unitPay
     * @param         $username
     */
    public function postDonate(Request $request, UnitPay $unitPay, $username)
    {
        $user = User::where('name', $username)->first();

        if ( ! $user) {
            abort(404);
        }

        $this->validate($request, [
            'username' => 'required|max:255',
            'sum' => 'required|numeric|min:' . $user->settings->get('donate_minimum_amount'),
            'speaker' => 'required',
            'emotion' => 'required',
            'message' => 'required|max:255'
        ]);

        $currency = 'RUB';
        $account = $request->get('username');
        $description = 'Оплата пожертвования пользователю ' . $user->name;

        $paymentUrl = $unitPay->paymentLink($account, $request->get('sum'), $description, [
            'user_id' => $user->id,
            'username' => $request->get('username'),
            'speaker' => $request->get('speaker'),
            'emotion' => $request->get('emotion'),
            'message' => $request->get('message'),
        ]);

        return redirect()->to($paymentUrl);
    }
}
