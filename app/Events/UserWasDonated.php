<?php

namespace App\Events;

use App\Donation;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserWasDonated extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * @var
     */
    public $show;

    /**
     * @var mixed
     */
    public $title;

    /**
     * @var mixed
     */
    public $message;

    /**
     * @var array
     */
    public $options;

    /**
     * @var
     */
    private $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Donation $donation)
    {
        $this->user = $donation->user;

        $this->show = $donation->amount >= $this->user->settings->get('donate_minimum_amount');
        $this->title = $this->getDonationTitle($donation);
        $this->message = $donation->message;
        $this->options = [
            'amount' => $donation->amount,
            'speaker' => $donation->speaker,
            'emotion' => $donation->emotion,
            'robot' => $donation->robot,
            'speech' => $donation->amount >= $this->user->settings->get('donation_alert_min_amount'),
            'donation_sound' => asset($this->user->settings->get('donation_alert_sound')),
            'donation_alert_animation' => asset($this->user->settings->get('donation_alert_animation')),
        ];
    }

    /**
     * @param Donation $donation
     *
     * @return mixed
     */
    private function getDonationTitle(Donation $donation)
    {
        $template = $this->user->settings->get('donation_alert_template');

        return str_replace([
            '{username}',
            '{amount}',
        ], [
            $donation->username,
            $donation->amount,
        ], $template);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [$this->user->name . '-channel'];
    }
}
