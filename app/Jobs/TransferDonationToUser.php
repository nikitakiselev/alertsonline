<?php

namespace App\Jobs;

use App\Donation;
use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\DigitalHammer\WebMoney\WebMoneyInterface;

class TransferDonationToUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var Donation
     */
    private $donation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $webMoney = app(WebMoneyInterface::class);
        $log = app(Log::class);
        $user = $this->donation->user;

        if ( ! $user->settings->has('withdraw_webmoney_purse')) {
            return $log->error("It is not possible to transfer money to the userId: " . $user->id . ", because it does not have Webmoney wallet!");
        }

        $result = $webMoney->moneyTransaction(
            $user->settings->get('withdraw_webmoney_purse'),
            $this->donation->amount
        );

        $this->donation->paid = $result;
        $this->donation->paid_at = Carbon::now();
        $this->donation->save();
    }
}
