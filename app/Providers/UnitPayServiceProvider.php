<?php

namespace App\Providers;

use App\DigitalHammer\UnitPay\UnitPay;
use Illuminate\Support\ServiceProvider;

class UnitPayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('unitpay', function() {
            return app(UnitPay::class);
        });
    }
}
