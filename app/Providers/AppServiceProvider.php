<?php

namespace App\Providers;

use App\Account;
use App\Donation;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::creating(function($user) {
            // generate user widget token
            $user->widget_token = hash_hmac('sha256', str_random(40), config('app.key'));
        });

        Donation::creating(function($donation) {
            $commission = config('alerts.commission') / 100;
            $donation->paid_sum = $donation->amount - ($donation->amount * $commission);
        });

        \Validator::extend('lat_alpha_dash', function($attribute, $value, $parameters, $validator) {
            if (! is_string($value) && ! is_numeric($value)) {
                return false;
            }

            return preg_match('/^[a-z\d_-]+$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
