<?php namespace App;

use App\DigitalHammer\UserSettings\UserSettingsTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use UserSettingsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'widget_token', 'active'
    ];

    /**
     * Available user settings
     *
     * @var array
     */
    protected $config = [
        'donate_text',
        'donate_bg',
        'account_avatar',
        'donation_alert_min_amount',
        'donation_alert_template',
        'donation_alert_sound',
        'donation_alert_animation',
        'donate_minimum_amount',
        'withdraw_webmoney_purse'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'settings' => 'collection'
    ];

    /**
     * User's donations
     */
    public function donations()
    {
        return $this->hasMany(Donation::class);
    }

    /**
     * Check user activation
     *
     * @return bool
     */
    public function activated()
    {
        return strlen($this->confirmation_code) == 0;
    }

    /**
     * Scope non banned and activated users
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query
            ->where('confirmation_code', null)
            ->where('active', true);
    }
}
