<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'amount',
        'message',
        'speaker',
        'emotion',
        'robot',
        'paid',
        'paid_at'
    ];

    /**
     * Custom date fields of donation model
     *
     * @var array
     */
    protected $dates = ['paid_at'];

    /**
     * Recipient donate user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope not paid donations
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotPaid($query)
    {
        return $query->where('paid', false);
    }

    /**
     * Get ready to pay donations
     *
     * @return mixed
     */
    public function scopeReadyForPayments()
    {
        return $this->notPaid()->whereHas('user', function($query) {
            $query->active();
        });
    }
}
