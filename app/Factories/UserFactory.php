<?php

namespace App\Factories;

use App\User;

class UserFactory
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create()
    {
        $this->user->create([
            // user data
        ]);

        // event(new UserWasCreated($user));
    }

    public function defaultSettings()
    {
        return [
            'donation.text' => 'Нет сообщения',
            'donation.image' => '',
        ];
    }

}