<?php

namespace App\Listeners;

use App\Events\UserWasRegister;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRegisterConfirmationEmail
{

    /**
     * @var Mailer
     */
    private $mailer;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegister  $event
     * @return void
     */
    public function handle(UserWasRegister $event)
    {
        $user = $event->user;

        $this->mailer->send('auth.emails.confirmation', compact('user'), function($message) use ($user) {
            $message
                ->to($user->email)
                ->subject('Подтверждение регистрации на сайте ' . config('alerts.site_name'));
        });
    }
}
