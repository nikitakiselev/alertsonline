<?php

namespace App\Console\Commands;

use App\Donation;
use Illuminate\Console\Command;
use App\Jobs\TransferDonationToUser;

class PaymentTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start user payment transaction';

    /**
     * @var Donation
     */
    private $donation;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Donation $donation)
    {
        parent::__construct();

        $this->donation = $donation;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $donations = $this->donation->readyForPayments()->get();

        $this->info('Find ' . $donations->count() . ' donations.');

        $donations->each(function($donation) {
            dispatch(new TransferDonationToUser($donation));
        });
    }
}
