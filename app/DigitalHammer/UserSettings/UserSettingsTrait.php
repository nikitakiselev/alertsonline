<?php

namespace App\DigitalHammer\UserSettings;

trait UserSettingsTrait
{

    /**
     * @param $key
     *
     * @return null
     */
    public function getFormValue($key)
    {
        if (in_array($key, $this->config))
        {
            return $this->settings->get($key);
        }

        return null;
    }

    /**
     * @param $settings
     *
     * @return Settings
     */
    public function getSettingsAttribute($settings)
    {
        return new Settings($this, json_decode($settings), $this->config);
    }
}