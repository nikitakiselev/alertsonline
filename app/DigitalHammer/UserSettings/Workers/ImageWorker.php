<?php

namespace App\DigitalHammer\UserSettings\Workers;

use Image;
use Symfony\Component\HttpFoundation\File\File;

class ImageWorker
{

    /**
     * @var UploadedFile
     */
    private $file;

    private $field;


    /**
     * ImageWorker constructor.
     */
    public function __construct(File $file, $field)
    {
        $this->file = $file;
        $this->field = $field;
    }

    public function proccess()
    {
        $width = config("user_settings.image_styles.$this->field.width");
        $height = config("user_settings.image_styles.$this->field.height");

        if ($width || $height) {
            $pathname = $this->file->getPathname();

            $image = Image::make($pathname);

            $image->fit(
                $width,
                $height,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            );

            $image->save($pathname, 80);
        }
    }
}