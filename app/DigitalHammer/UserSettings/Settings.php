<?php

namespace App\DigitalHammer\UserSettings;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Settings
{
    private $settings;
    private $user;
    private $availableSettings;

    /**
     * Settings constructor.
     *
     * @param $user
     * @param $settings
     * @param $availableSettings
     */
    public function __construct($user, $settings, $availableSettings)
    {
        $this->user = $user;
        $this->settings = collect((array) $settings);
        $this->availableSettings = $availableSettings;
    }

    /**
     * Get all  user settings
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->settings;
    }

    /**
     * Get user settings by Id
     *
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        if ($item = $this->settings->get($key))
        {
            return $this->settings->get($key);
        }

        return $this->getDefalt($key);
    }


    /**
     * Получает настройку по умолчанию
     *
     * @param $key
     *
     * @return mixed|null
     */
    public function getDefalt($key)
    {
        $setting = config('user_settings.defaults.' . $key);


        return $setting ? $setting : null;
    }

    /**
     * Set user settings
     *
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function set($key, $value)
    {
        // get field type
        switch($this->getFieldType($value))
        {
            case 'file':
                $uploader = new FileUploader($value, $key, $this->user->name);
                $filename = $uploader->upload();

                return $this->user->settings = $this->settings->put($key, $filename);
                break;

            default:
                return $this->user->settings = $this->settings->put($key, $value);
                break;
        }


    }

    /**
     * Remove all user settings
     *
     * @return array
     */
    public function clear()
    {
        return $this->user->settings = [];
    }

    /**
     * Update user's settings
     *
     * @param $params
     */
    public function update($params)
    {
        foreach($this->availableSettings as $key)
        {
            if (isset($params[$key])) {

                $field = $params[$key];

                $this->set($key, $field);
            }
        }

        return $this->user;
    }

    /**
     * @return string
     */
    public function getFieldType($field)
    {
        if ($field instanceof UploadedFile) {
            return 'file';
        }

        return 'string';
    }

    /**
     * Check for user set settings
     *
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->settings->has($key);
    }

}