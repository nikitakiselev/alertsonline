<?php

namespace App\DigitalHammer\UserSettings;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @var UploadedFile
     */
    private $file;

    private $field;

    private $username;


    /**
     * FileUploader constructor.
     *
     * @param UploadedFile $file
     */
    public function __construct(UploadedFile $file, $field, $username)
    {
        $this->file = $file;
        $this->field = $field;
        $this->username = $username;
    }


    /**
     * @return string
     */
    public function upload()
    {
        if ($this->file->isValid())
        {
            $type = explode('/', $this->file->getMimeType())[0];
            $path = 'userdata/' . strtolower($this->username) . '/' . $type;
            $destinationPath = public_path($path);
            $fileExtenshion = $this->file->getClientOriginalExtension();
            $fileName = $this->field . '.' . $fileExtenshion;

            // move file
            $file = $this->file->move($destinationPath, $fileName);

            $workerClassName = 'App\\DigitalHammer\\UserSettings\\Workers\\' . ucfirst(strtolower($type)) . 'Worker';

            if (class_exists($workerClassName))
            {
                $worker = new $workerClassName($file, $this->field);
                $worker->proccess();
            }

            // return path to file
            return $path . '/' . $fileName;
        }
    }
}