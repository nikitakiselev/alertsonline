<?php

namespace App\DigitalHammer\WebMoney;

use baibaratsky\WebMoney\Signer;
use baibaratsky\WebMoney\WebMoney;
use baibaratsky\WebMoney\Api\X\X2;
use Illuminate\Contracts\Logging\Log;
use baibaratsky\WebMoney\Request\Requester\CurlRequester;

class WebMoneyInterface
{
    /**
     * @var Log
     */
    private $log;

    /**
     * WebMoneyInterface constructor.
     *
     * @param Log $log
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * Money transaction to another purse
     *
     * @param        $payeePurse
     * @param        $amount
     * @param string $description
     *
     * @throws \baibaratsky\WebMoney\Exception\CoreException
     */
    public function moneyTransaction($payeePurse, $amount, $description = '')
    {
        $amount = number_format($amount, 2);

        $webMoney = new WebMoney(new CurlRequester);

        $request = new X2\Request;
        $request->setSignerWmid(config('payment.webmoney.wmid'));
        $request->setTransactionExternalId(1); // Unique ID of the transaction in your system
        $request->setPayerPurse(config('payment.webmoney.purse'));
        $request->setPayeePurse($payeePurse);
        $request->setAmount($amount); // Payment amount
        $request->setDescription($description);

        $request->sign(new Signer(config('payment.webmoney.wmid'), config('payment.webmoney.key_file'),
                config('payment.webmoney.key_password')));

        if ( ! $request->validate()) {
            $this->log->error('Webmoney transaction validation error', $request->getErrors());

            return false;
        }

        $response = $webMoney->request($request);

        if ($response->getReturnCode() === 0) {
            $this->log->info('Webmoney successful payment, transaction id: ' . $response->getTransactionId());

            return true;
        }

        $this->log->error('Webmoney payment error', $request->getReturnDescription());

        return false;
    }
}