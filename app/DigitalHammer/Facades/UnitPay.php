<?php namespace App\DigitalHammer\Facades;

use Illuminate\Support\Facades\Facade;

class UnitPay extends Facade {

    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'unitpay';
    }

}