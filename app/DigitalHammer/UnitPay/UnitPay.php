<?php

namespace App\DigitalHammer\UnitPay;

class UnitPay
{
    const DEFAULT_CURRENCY = 'RUB';

    private $supportedCurrencies = [ 'EUR', 'UAH', 'BYR', 'USD', 'RUB' ];

    private $supportedUnitpayMethods = [ 'initPayment', 'getPayment' ];

    private $requiredUnitpayMethodsParams = [
        'initPayment' => [ 'desc', 'account', 'sum' ],
        'getPayment'  => [ 'paymentId' ]
    ];

    private $supportedPartnerMethods = [ 'check', 'pay', 'error' ];

    private $supportedUnitpayIp = [
        '31.186.100.49',
        '178.132.203.105',
        '127.0.0.1' // for debug
    ];

    private $apiUrl = 'https://unitpay.ru/api';

    private $formUrl = 'https://unitpay.ru/pay/';

    /**
     * @var null
     */
    private $secretKey;

    /**
     * @var null
     */
    private $publicKey;


    /**
     * UnitPay constructor.
     *
     * @param null $secretKey
     * @param null $publicKey
     */
    public function __construct()
    {
        $this->secretKey = config('payment.unitpay.secret_key');
        $this->publicKey = config('payment.unitpay.public_key');

        $this->log = $this->getLoggerInstance();
    }

    /**
     * Logger
     *
     * @return mixed
     */
    protected function getLoggerInstance()
    {
        return app()->make(\Illuminate\Contracts\Logging\Log::class);
    }

    /**
     * Create digital signature
     *
     * @param array $params
     *
     * @return string
     */
    private function getMd5sign($params)
    {
        ksort($params);
        unset( $params['sign'] );

        return md5(join(null, $params) . $this->secretKey);
    }

    /**
     * Get payment form action link
     *
     * @return string
     */
    public function paymentFormActionLink()
    {
        return $this->formUrl . $this->publicKey;
    }

    /**
     * Get paymeny url
     *
     * @param       $account
     * @param       $sum
     * @param       $description
     * @param array $parameters
     *
     * @return string
     */
    public function paymentLink($account, $sum, $description, $parameters = [])
    {
        $sum = number_format($sum, 2);

        $currency = isset($parameters['currency'])
            ? $parameters['currency']
            : self::DEFAULT_CURRENCY;

        $sign = $this->getMd5sign([
            $account,
            $currency,
            $description,
            $sum,
            $this->secretKey
        ]);

        $queryParams = [
            'account' => $account,
            'sum' => $sum,
            'desc' => $description,
            'sign' => $sign,
            'currency' => 'RUB',
        ];

        $queryParams = array_merge([], $queryParams, $parameters);

        return $this->paymentFormActionLink() . '?' . http_build_query($queryParams);
    }

    /**
     * Call API
     *
     * @param       $method
     * @param array $params
     *
     * @return object
     *
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     */
    public function api($method, $params = [ ])
    {
        if ( ! in_array($method, $this->supportedUnitpayMethods)) {
            throw new UnexpectedValueException('Method is not supported');
        }
        if (isset( $this->requiredUnitpayMethodsParams[$method] )) {
            foreach ($this->requiredUnitpayMethodsParams[$method] as $rParam) {
                if ( ! isset( $params[$rParam] )) {
                    throw new InvalidArgumentException('Param ' . $rParam . ' is null');
                }
            }
        }
        $params['secretKey'] = $this->secretKey;
        if (empty( $params['secretKey'] )) {
            throw new InvalidArgumentException('SecretKey is null');
        }
        $requestUrl = $this->apiUrl . '?' . http_build_query([
                'method' => $method,
                'params' => $params
            ], null, '&', PHP_QUERY_RFC3986);
        $response   = json_decode(file_get_contents($requestUrl));
        if ( ! is_object($response)) {
            throw new InvalidArgumentException('Temporary server error. Please try again later.');
        }

        return $response;
    }


    /**
     * Check request on handler from UnitPay
     *
     * @return bool
     *
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     */
    public function checkHandlerRequest()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if ( ! isset( $_GET['method'] )) {
            throw new InvalidArgumentException('Method is null');
        }
        if ( ! isset( $_GET['params'] )) {
            throw new InvalidArgumentException('Params is null');
        }
        list( $method, $params ) = [ $_GET['method'], $_GET['params'] ];
        if ( ! in_array($method, $this->supportedPartnerMethods)) {
            throw new UnexpectedValueException('Method is not supported');
        }
        if ($params['sign'] != $this->getMd5sign($params)) {
            throw new InvalidArgumentException('Wrong signature');
        }

        /**
         * IP address check
         * @link https://unitpay.ru/doc#overview
         */
        if ( ! $this->isAllowedIp($ip)) {
            throw new InvalidArgumentException('IP address Error');
        }

        return true;
    }


    /**
     * @param $ip
     *
     * @throws InvalidArgumentException
     */
    public function isAllowedIp($ip)
    {
        return in_array($ip, $this->supportedUnitpayIp);
    }


    /**
     * Response for UnitPay if handle success
     *
     * @param $message
     *
     * @return string
     */
    public function getSuccessHandlerResponse($message)
    {
        $this->log->info('UnitPay: ' . $message);

        return response()->json([
            "result" => [
                "message" => $message
            ]
        ]);
    }


    /**
     * Response for UnitPay if handle error
     *
     * @param $message
     *
     * @return string
     */
    public function getErrorHandlerResponse($message)
    {
        $this->log->error('UnitPay: ' . $message);

        return response()->json([
            "error" => [
                "message" => $message
            ]
        ]);
    }
}