;(function(sm, window, socket) {
    sm.setup({
        debugMode: false,
        onready: function() {},
        ontimeout: function() {
            alert('Uh-oh. No HTML5 support, SWF missing, Flash blocked or other issue');
        }
    });

    function Player(soundManager, yandexDevKey) {
        // sound manager
        this.soundManager = soundManager;

        // Default yandex spech options
        this.defaultSpechOptions = {
            speaker: 'zahar',
            emotion: 'good',
            robot: false
        };

        this.yandexDevKey = yandexDevKey;
    }

    /*
     * Generate playeble link
     */
    Player.prototype.generateSpechLink = function(message, options) {
        options = Object.assign({}, this.defaultSpechOptions, options);

        return 'https://tts.voicetech.yandex.net/generate?text=' +
            encodeURIComponent(message) +
            '&format=mp3' +
            '&lang=' + encodeURIComponent('ru-RU') +
            '&speaker=' + options.speaker +
            '&emotion=' + options.emotion +
            '&robot=' + options.robot +
            '&key=' + this.yandexDevKey;
    };

    Player.prototype.playAlert = function(message, options, onFinish) {
        var player = this;

        var message = soundManager.createSound({
            id: 'message',
            autoLoad: true,
            url: player.generateSpechLink(message, options),
            onfinish: function() {
                player.soundManager.destroySound('notification');
                player.soundManager.destroySound('message');
                onFinish.call();
            }
        });

        var notification = soundManager.createSound({
            id: 'notification',
            url: options.donation_sound,
            onfinish: function() {
                message.play();
            }
        });

        notification.play();
    };

    /**
     * class Alert
     */
    function Alert(player) {
        this.player = player;
        this.queue = [];
        this.working = false;

        this.dom = {
            wrapper: document.getElementById('alert-wrapper'),
            title: document.getElementById('donation-title'),
            message: document.getElementById('donation-message'),
            image: document.getElementById('donation-image')
        };
    }

    /**
     * Add message to queue
     */
    Alert.prototype.show = function(data) {
        if (data.show) {
            this.queue.push({
                title: data.title,
                message: data.message,
                options: data.options
            });

            console.log('Add to queue', data);
            this.startQueue();
        }
    };

    /**
     * Do job
     */
    Alert.prototype.job = function(object) {
        var self = this;

        console.log('Job: ', object);

        self.working = true;

        // show message
        self.dom.title.innerHTML = object.title;
        self.dom.message.innerHTML = object.message;
        self.dom.image.setAttribute('src', object.options.donation_alert_animation);
        self.dom.image.setAttribute('class', '');
        self.showAlert();

        setTimeout(function() {
            self.hideAlert();

            if (!object.options.speech) {
                self.next();
            }

        }, self.getTextShowTime(object.message));

        // play sound and speech text
        if (object.options.speech) {
            self.player.playAlert(object.message, object.options, function() {
                self.next();
            });
        }
    };

    /**
     * Next queue alert
     */
    Alert.prototype.next = function() {
        var alert = this;
        setTimeout(function() {
            alert.working = false;
            alert.startQueue();
        }, 5000);
    };

    /**
     * Start alert queue
     */
    Alert.prototype.startQueue = function() {
        console.log('Queue: ', this.queue);
        if (!this.working && this.queue.length) {
            this.job(
                this.queue.shift()
            );
        }
    };

    /**
     * Hide alert from screen
     */
    Alert.prototype.hideAlert = function() {
        this.dom.wrapper.className = 'fadeOut animated';
    };

    Alert.prototype.showAlert = function() {
        this.dom.wrapper.className = 'fadeIn animated';
    };

    Alert.prototype.getTextShowTime = function(text) {
        var time = text.length * 15000 / 255;
        return time > 5000 ? time : 5000;
    };


    var player = new Player(sm, window.devKey),
        alert = new Alert(player);

    socket.on(window.username + "-channel:App\\Events\\UserWasDonated", function(data) {
        console.log('New event: ', data);
        alert.show(data);
    });
})(soundManager, window, socket);