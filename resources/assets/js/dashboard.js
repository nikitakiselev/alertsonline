;(function($) {

    $('[data-toggle="popover"]').popover({
        container: 'body',
        html: true,
        trigger: 'click'
    });

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    var $testDonateAlertButton = $('#test-donate-alert'),
        timeout = null,
        timer = null,
        waitTime = 1000;

    function sendDonateTestRequest() {
        $.get(window.testDonateAlertUrl, function(){})
            .error(function() {
                alert('Возникла ошибка');
            });
        clearInterval(timer);
        $testDonateAlertButton.button('reset');
    }

    $testDonateAlertButton.on('click', function(e) {
        var $this = $(this),
            wait = waitTime;

        clearTimeout(timeout);
        clearInterval(timer);

        $this
            .attr('data-loading-text', wait / 1000)
            .button('loading');

        timeout = setTimeout(sendDonateTestRequest, waitTime);
        timer = setInterval(function() {
            wait -= 1000;
            $this.text(wait / 1000);
        }, 1000);
        e.preventDefault();
    });

})(jQuery);