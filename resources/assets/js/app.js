;(function($) {

    $('[data-toggle="popover"]').popover({
        container: 'body',
        html: true,
        trigger: 'hover'
    });

})(jQuery);