<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <img class="avatar" src="{{ asset(Auth::user()->settings->get('account_avatar')) }}" alt="Avatar">
        {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="fa fa-btn fa-fw fa-dashboard"></i> Панель управления
            </a>
        </li>
        <li>
            <a href="{{ route('donations') }}">
                <i class="fa fa-btn fa-fw fa-gift"></i> Мои донаты
            </a>
        </li>

        <li>
            <a href="{{ route('dashboard.settings') }}">
                <i class="fa fa-btn fa-fw fa-cog"></i> Настройки
            </a>
        </li>

        <!-- Logout -->
        <li class="divider"></li>

        <li>
            <a href="/logout">
                <i class="fa fa-btn fa-fw fa-sign-out"></i> Выход
            </a>
        </li>
    </ul>
</li>