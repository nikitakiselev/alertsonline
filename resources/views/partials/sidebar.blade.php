<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(Auth::user()->settings->get('account_avatar')) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Мой аккаунт</li>
            <!-- Optionally, you can add icons to the links -->
            <li {!! request()->url() == route('dashboard') ? 'class="active"' : '' !!}><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Панель управления</span></a></li>
            <li {!! request()->url() == route('donations') ? 'class="active"' : '' !!}><a href="{{ route('donations') }}"><i class="fa fa-gift"></i> <span>Мои донаты</span></a></li>

            <li class="treeview {!! in_array(request()->url(), [route('dashboard.settings'), route('dashboard.settings.donate'), route('dashboard.settings.withdrawal')]) ? ' active' : '' !!}">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>Настройки</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li class="{!! request()->url() == route('dashboard.settings') ? ' active' : '' !!}">
                        <a href="{{ route('dashboard.settings') }}"><i class="fa fa-user"></i> Аккаунт</a>
                    </li>

                    <li class="{!! request()->url() == route('dashboard.settings.donate') ? ' active' : '' !!}">
                        <a href="{{ route('dashboard.settings.donate') }}"><i class="fa fa-credit-card"></i> Донат</a>
                    </li>

                    <li class="{!! request()->url() == route('dashboard.settings.withdrawal') ? ' active' : '' !!}">
                        <a href="{{ route('dashboard.settings.withdrawal') }}"><i class="fa fa-money"></i> Вывод средств</a>
                    </li>
                </ul>
            </li>

            <li class="header">Виджеты</li>
            <li {!! request()->url() == route('widget.donation') ? 'class="active"' : '' !!}><a href="{{ route('widget.donation') }}"><i class="fa fa-users"></i> <span>Донат</span></a></li>
        </ul>
    </section>
</aside>