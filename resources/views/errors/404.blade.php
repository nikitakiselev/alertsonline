@extends('layouts.app')

@section('header', 'Страница не найдена')

@section('content')
    <div class="container">
        <h2>Страница не найдена</h2>
        <p>Адрес, по которому Вы перешли или не существует или уже не действителен.</p>
    </div>
@endsection