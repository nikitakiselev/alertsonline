@extends('layouts.app')

@section('header', 'Аккаунт заблокирован')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>Ваш аккаунт заблокирован.</h2>
                <p>Обратитесь в техподдержку для выяснения причины блокировки.</p>
                <p><a href="mailto:{{ config('alerts.site_email') }}">{{ config('alerts.site_email') }}</a></p>
            </div>
        </div>
    </div>
@endsection