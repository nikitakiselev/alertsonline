@extends('layouts.app')

@section('header', 'Подтверждение регистрации')

@section('content')
    <fieldset class="confirmation-text">
        <h2>Ваш аккаунт зарегистрирован.</h2>
        <p>Для его активации необходимо перейти по ссылке, отправленной Вам в письме на указанный при регистрации адрес e-mail.</p>
    </fieldset>
@endsection