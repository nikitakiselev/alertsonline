@extends('layouts.app')

@section('header', 'Подтверждение регистрации')

@section('content')
    <fieldset class="confirmation-text">
        <p>Вы успешно подтвердили свой e-mail адрес. Теперь Вы можете перейти в свой личный кабинет по ссылке ниже:</p>
        <p><a href="/dashbard">Перейти в личный кабинет</a></p>
    </fieldset>

@endsection