@extends('layouts.app')

@section('header', 'Аккаунт не активирован')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>Вы не активировали свой аккаунт.</h2>
                <p>При регистрации на указанный адрес email Вам было отправлено писмо от "Alerts Online" со ссылкой для подтверждения адреса и активации аккаунта.</p>
                <p>По всей видимости Вы этого не сделали, поэтому видите сейчас это страницу.</p>
                <p>Найдите письмо и перейдите по ссылке в нём или обратитесь в техподдержку для помощи в активации аккаунта.</p>
                <p><a href="mailto:{{ config('alerts.site_email') }}">{{ config('alerts.site_email') }}</a></p>
            </div>
        </div>
    </div>
@endsection