@extends('layouts.app')

@section('header', 'Сброс пароля')

@section('content')
    <form class="center" action="{{ url('/password/reset') }}" method="POST">
        {!! csrf_field() !!}

        <fieldset class="registration-form">

            <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="controls">
                    <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="E-mail">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="control-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="controls">
                    <input type="password" class="form-control" name="password" value="" placeholder="Пароль">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="Подтверждение пароля">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success btn-large btn-block"><i class="fa fa-btn fa-refresh"></i> Сбросить пароль</button>
                </div>
            </div>
        </fieldset>
    </form>
@endsection