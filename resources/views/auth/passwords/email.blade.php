@extends('layouts.app')

@section('header', 'Восстановление пароля')

@section('content')
    <form class="center" action="{{ url('/password/email') }}" method="POST">
        {!! csrf_field() !!}

        <fieldset class="registration-form">

            <div class="alert alert-info">
                Введите свой e-mail в поле ниже. На него придёт ссылка для сброса пароля.
            </div>

            <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="controls">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-envelope"></i>Выслать ссылку
                    </button>
                </div>
            </div>
        </fieldset>
    </form>
@endsection