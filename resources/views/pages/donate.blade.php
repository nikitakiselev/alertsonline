<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пожертвование для {{ $username }} - {{ config('alerts.site_name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/donate.css') }}"/>
</head>
<body>
    <div class="donation-wrapper">
        <div class="donation-header" style="background-image: url({{ asset($user->settings->get('donate_bg')) }});">
            <div class="donation-meta">
                {{ $username }}
            </div>
        </div>

        <div class="donation-body">

            <div class="donation-text">{{ $user->settings->get('donate_text') }}</div>

            {!! Form::open(['method' => 'post']) !!}
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                {!! Form::label('username', 'Имя пользователя') !!}
                                {!! Form::text('username', $username, ['class' => 'form-control', 'readonly']) !!}
                                <div class="help-block">{{ $errors->first('username') }}</div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('sum') ? ' has-error' : '' }} has-feedback">
                                {!! Form::label('sum', 'Сумма (руб)') !!}
                                {!! Form::input('number', 'sum', $user->settings->get('donation_alert_min_amount'), ['class' => 'form-control', 'min' => Auth::user()->settings->get('donate_minimum_amount')]) !!}
                                <i class="icon-rouble form-control-feedback"></i>
                                <div class="help-block">{{ $errors->first('sum') }}</div>
                                <div class="help-block">
                                    Минимальная сумма, при которой Ваше сообщение воспроизводится голосом - <strong>{{ $user->settings->get('donation_alert_min_amount') }} рублей</strong>.
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="speaker" class="control-label">Голос</label>
                                <select name="speaker" id="speaker" class="form-control">
                                    @foreach(config('yandex.speech.speakers') as $speaker)
                                        <option value="{{ $speaker }}">
                                            {{ trans('yandex.speech.speakers.' . $speaker) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="emotion" class="control-label">эмоциональная окраска голоса</label>
                                <select name="emotion" id="emotion" class="form-control">
                                    @foreach(config('yandex.speech.emotions') as $emotion)
                                        <option value="{{ $emotion }}">
                                            {{ trans('yandex.speech.emotions.' . $emotion) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="robot" class="control-label">
                            <input type="checkbox" id="robot" name="robot" value="true"/> эмуляция механического голоса
                        </label>
                    </div>

                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label for="message" class="control-label">Сообщение</label>
                        <textarea name="message" id="message" class="form-control" maxlength="255"></textarea>
                        <div class="help-block">Осталось символов: <span id="count" class="badge">255</span>.</div>
                        <div class="help-block">{{ $errors->first('message') }}</div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="submit-btn">Отправить</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        document.getElementById('message').onkeyup = function () {
            document.getElementById('count').innerHTML = (255 - this.value.length);
        };
    </script>
</body>
</html>