<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('alerts.site_name') }} лучший сервис онлайн пожертвований</title>

    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href="{{ asset(elixir('css/app.css')) }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container spark-splash-screen">
    <div class="row splash-nav">
        <div class="col-md-10 col-md-offset-1">
            <div class="pull-left splash-brand">
                <a href="/" class="brand">
                    <i class="fa fa-btn fa-sun-o"></i> {{ config('alerts.site_name') }}
                </a>
            </div>

            <div class="navbar-header">
                <button type="button" class="splash-nav-toggle navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-nav" aria-expanded="false" aria-controls="primary-nav">
                    <span class="sr-only">Toggle navigation</span>
                    MENU
                </button>
            </div>

            <div id="primary-nav" class="navbar-collapse collapse splash-nav-list">
                <ul class="nav nav-pills navbar-right inline-list">
                    <li class="splash-nav-link"><a href="/features">О сервисе</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Сообщество <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="/how-to">
                                    <i class="fa fa-btn fa-fw fa-cog"></i> Как настроить
                                </a>
                            </li>

                            <li>
                                <a href="/faq">
                                    <i class="fa fa-btn fa-fw fa-question"></i> FAQ
                                </a>
                            </li>

                            <li>
                                <a href="/comunity">
                                    <i class="fa fa-btn fa-fw fa-archive"></i> Статьи
                                </a>
                            </li>
                        </ul>
                    </li>


                    @if (Auth::check())
                        @include('site.nav.settings')
                    @else
                        <li class="splash-nav-link splash-nav-link-highlight"><a href="/login">Вход</a></li>
                        <li class="splash-nav-link splash-nav-link-highlight-border"><a href="/register">Регистрация</a></li>
                    @endif
                </ul>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Inspiration -->
    <div class="row splash-inspiration-row">
        <div class="col-md-4 col-md-offset-1">
            <div id="splash-inspiration-heading">
                Онлайн оповещения на экране!
            </div>

            <div id="splash-inspiration-text">
                Выводит сообщения о донате на экран
            </div>
        </div>

        <!-- Browser Window -->
        <div class="col-md-6" class="splash-browser-window-container">
            <div class="splash-browser-window">
                <div class="splash-browser-dots-container">
                    <ul class="list-inline splash-browser-dots">
                        <li><i class="fa fa-circle red"></i></li>
                        <li><i class="fa fa-circle yellow"></i></li>
                        <li><i class="fa fa-circle green"></i></li>
                    </ul>
                </div>
                <div>
                    <img src="https://placehold.co/550x400" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>

    <!-- Features Heading -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1 splash-row-heading">
            Что вы получаете
        </div>
    </div>

    <!-- Feature Icons -->
    <div class="row splash-features-icon-row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-clock-o"></i>
                </div>

                <div class="splash-feature-heading">
                    Простая настройка
                </div>

                <div class="splash-feature-text">
                    Мы постарались сделать сервис максимально простым и удобным. Если всё же возникли трудности, вы всегда можете обратиться в сообщество.
                </div>
            </div>

            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-phone"></i>
                </div>

                <div class="splash-feature-heading">
                    Не только Twitch
                </div>

                <div class="splash-feature-text">
                    Alerts Online не привязан к какому либо сервису. Вы легко можете вести трансляции на youtube, livecoding и на любые другие сайты.
                </div>
            </div>

            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-money"></i>
                </div>

                <div class="splash-feature-heading">
                    Минимальная комиссия
                </div>

                <div class="splash-feature-text">
                    Каким большим не было бы наше желание сделать пожертвования без комиссии, но то не возможно. Но мы нашли платёжную систему с минимальной комиссией.
                </div>
            </div>
        </div>
    </div>

    <!-- Feature Icons -->
    <div class="row splash-features-icon-row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-users"></i>
                </div>

                <div class="splash-feature-heading">
                    Продвинутый синтезатор речи
                </div>

                <div class="splash-feature-text">
                    Alerts Online использует лучший синтезатор речи, который позволяет максимально качественно воспроизводить сообщения от пользователей.
                </div>
            </div>

            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-cubes"></i>
                </div>

                <div class="splash-feature-heading">
                    О Вас узнают!
                </div>

                <div class="splash-feature-text">
                    Хотите найти себе новых подписчиков? Разместите свою информацию на нашем сайте, и все, кто зайдят на сайт сможет увидеть на Ваш канал.
                </div>
            </div>

            <div class="col-md-4 splash-features-feature">
                <div class="splash-feature-icon">
                    <i class="fa fa-lock"></i>
                </div>

                <div class="splash-feature-heading">
                    Кастомизация формы доната
                </div>

                <div class="splash-feature-text">
                    Мы сделали так, чтобы Вы могли гибко настраивать под себя форму доната, которую видят пользователи.
                </div>
            </div>
        </div>
    </div>

    <!-- Call To Action Button -->
    <div class="row call-to-action-row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <a href="/register">
                <button class="btn btn-primary splash-get-started-btn">
                    <i class="fa fa-rocket"></i> Начать работу!
                </button>
            </a>
        </div>
    </div>

            <!-- Customers Heading -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1 splash-row-heading">
            С нами работают
        </div>
    </div>

    <!-- Customer Testimonials -->
    <div class="row splash-customer-row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <div class="col-md-4 splash-customer">
                <div class="splash-customer-avatar">
                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/msurguy/128.jpg">
                </div>

                <div class="splash-customer-quote">
                    This is an inspiring testimonial about your application.
                </div>

                <div class="splash-customer-identity">
                    <div class="splash-customer-name">Maksim Surguy</div>
                    <div class="splash-customer-title">CEO, Company</div>
                </div>
            </div>

            <div class="col-md-4 splash-customer">
                <div class="splash-customer-avatar">
                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/allisongrayce/128.jpg">
                </div>

                <div class="splash-customer-quote">
                    This is an inspiring testimonial about your application.
                </div>

                <div class="splash-customer-identity">
                    <div class="splash-customer-name">Allison Grayce</div>
                    <div class="splash-customer-title">CEO, Company</div>
                </div>
            </div>

            <div class="col-md-4 splash-customer">
                <div class="splash-customer-avatar">
                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/richcsmith/128.jpg">
                </div>

                <div class="splash-customer-quote">
                    This is an inspiring testimonial about your application.
                </div>

                <div class="splash-customer-identity">
                    <div class="splash-customer-name">Rich Smith</div>
                    <div class="splash-customer-title">CEO, Company</div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="row">
        <!-- Company Information -->
        <div class="col-md-10 col-md-offset-1 splash-footer">
            <div class="pull-left splash-footer-company">
                Copyright © {{ config('alerts.site_name') }} - <a href="/terms">Условия использования</a>
            </div>

            <!-- Social Icons -->
            <div class="pull-right splash-footer-social-icons">
                <a href="http://facebook.com">
                    <i class="fa fa-btn fa-facebook-square"></i>
                </a>
                <a href="http://twitter.com">
                    <i class="fa fa-btn fa-twitter-square"></i>
                </a>
                <a href="http://github.com">
                    <i class="fa fa-github-square"></i>
                </a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>

<!-- Footer Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>