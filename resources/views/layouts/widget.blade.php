<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title', 'Donation Widget')</title>
    <link href='https://fonts.googleapis.com/css?family=Marmelad&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet">
    @yield('meta')
</head>
<body>
    @yield('content')
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
        var socket = io('{{ config('app.url') }}:6001'),
            devKey = "{{ config('yandex.dev_key') }}",
            username = "{{ $user->name }}";
    </script>
    @yield('scripts')
</body>
</html>