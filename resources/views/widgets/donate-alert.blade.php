@extends('layouts.widget')

@section('title', 'Donation Alert Widget')

@section('content')
    <div id="alert-wrapper">
        <div class="donation-image-wrapper">
            <img src="" alt="" id="donation-image"/>
        </div>
        <div id="donation-title"></div>
        <div id="donation-message"></div>
    </div>
@stop

@section('meta')
<link href="{{ asset(elixir('css/donation.css')) }}" rel="stylesheet">
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/soundmanager2/2.97a.20150601/script/soundmanager2-jsmin.js"></script>
    <script src="{{ asset(elixir('js/widgets/donation.js')) }}"></script>
@stop