@extends('layouts.dashboard')

@section('page-header')
    <h1><i class="fa fa-dashboard"></i> Панель управления <small>Информация и статистика</small></h1>
@stop

@section('content')

    <div class="alert alert-info">Страница с информацией и статистикой будет совершенствоваться.</div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Последние пожертвования</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($lastDonations as $donation)
                            <li class="item">
                                <div class="product-info">
                                    <div class="product-title">{{ $donation->username }}
                                        <span class="label label-warning pull-right">
                                            {{ $donation->sum }} <i class="fa fa-rub"></i>
                                        </span>
                                    </div>

                                    <span class="product-description">
                                      {{ str_limit($donation->message, 50) }}
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ route('donations') }}" class="uppercase">Посмотреть все донаты</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
@stop