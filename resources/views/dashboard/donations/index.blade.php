@extends('layouts.dashboard')

@section('title', 'Мои пожертвования - ' . config('alerts.site_name'))

@section('page-header')
    <h1><i class="fa fa-gift"></i> Мои пожертвования</h1>
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
        <li class="active">Мои донаты</li>
    </ol>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <table class="table table-striped table-bordered table-hover table-condensed table-responsive">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Логин пользователя</th>
                    <th>Сумма</th>
                    <th>Дата</th>
                    <th>Сообщение</th>
                    <th>Выплата</th>
                </tr>
                </thead>

                <tbody>
                @foreach($donations as $donation)
                    <tr>
                        <td width="5%">{{ $donation->id }}</td>
                        <td width="25%">{{ $donation->username }}</td>
                        <td width="7%">{{ $donation->amount }} <i class="fa fa-rub"></i></td>
                        <td width="15%">{{ $donation->created_at->format('d.m.Y H:i') }}</td>
                        <td>{{ $donation->message }}</td>
                        <td width="10%">
                            @if ($donation->paid)
                                <span class="label label-success" title="Дата: {{ $donation->paid_at->format('d.m.Y в H:i') }}" data-toggle="tooltip">
                                    <i class="fa fa-check"></i> {{ $donation->paid_sum }} <i class="fa fa-rub"></i>
                                </span>
                            @else
                                <span class="label label-warning" title="В очереди" data-toggle="tooltip">
                                    <i class="fa fa-ban"></i> {{ $donation->paid_sum }} <i class="fa fa-rub"></i>
                                </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {!! $donations->render() !!}
@stop