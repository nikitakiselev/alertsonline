@extends('layouts.dashboard')

@section('title', 'Настройки аккаунта - ' . config('alerts.site_name'))

@section('page-header')
    <h1><i class="fa fa-user"></i> Настройки аккаунта</h1>
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
        <li class="active">Настройки аккаунта</li>
    </ol>
@stop

@section('content')

    <div class="nav-tabs-custom">
        @include('dashboard.settings.tabs')

        <div class="tab-content">
            <div class="tab-pane active">
                {!! Form::open(['files' => true]) !!}
                    <div class="form-group{{ $errors->has('account_avatar') ? ' has-error' : '' }}">
                        <div class="media">
                            <div class="media-left">
                                <img src="{{ asset(Auth::user()->settings->get('account_avatar')) }}" alt="Avatar" class="media-object">
                            </div>
                            <div class="media-body">
                                {!! Form::label('account_avatar', 'Аватар') !!}
                                {!! Form::input('file', 'account_avatar') !!}
                                <div class="help-block">{{ $errors->first('account_avatar') }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop