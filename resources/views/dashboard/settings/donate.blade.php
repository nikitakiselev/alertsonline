@extends('layouts.dashboard')

@section('title', 'Настройки доната - ' . config('alerts.site_name'))

@section('page-header')
    <h1><i class="fa fa-credit-card"></i> Настройки доната</h1>
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
        <li class="active">Настройки доната</li>
    </ol>
@stop

@section('content')
    <div class="nav-tabs-custom">
        @include('dashboard.settings.tabs')

        <div class="tab-content">
            <div class="tab-pane active">
                {!! Form::model(Auth::user(), ['class' => 'form-horizontal', 'files' => true]) !!}
                    <div class="form-group">
                        <label for="donate-link" class="control-label col-sm-3">Ваша ссылка для доната</label>

                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" class="form-control" id="donate-link" value="{{ route('donate', strtolower(Auth::user()->name)) }}" readonly="readonly" onclick="jQuery(this).select();">
                                <div class="input-group-btn">
                                    <a href="{{ route('donate', strtolower(Auth::user()->name)) }}" class="btn btn-default" target="_blank" title="Открыть ссылку"><i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="donate_minimum_amount" class="control-label col-sm-3">Минимальная сумма пожертвования (руб.)</label>

                        <div class="col-sm-2">
                            {!! Form::input('number', 'donate_minimum_amount', null, ['class' => 'form-control', 'min' => 1, 'id' => 'donate_minimum_amount']) !!}
                            <div class="help-block">{{ $errors->first('donate_minimum_amount') }}</div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('donate_bg') ? ' has-error' : '' }}">
                        {!! Form::label('donate_bg', 'Фоновое изображение', ['class' => 'control-label col-sm-3']) !!}

                        <div class="col-sm-9">
                            @if (Auth::user()->settings->get('donate_bg'))
                                <img src="{{ asset(Auth::user()->settings->get('donate_bg')) }}" alt="Donate background" height="50"/>
                                <br><br>
                            @endif

                            {!! Form::input('file', 'donate_bg') !!}
                            <div class="help-block">{{ $errors->first('donate_bg') }}</div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('donate_text') ? ' has-error' : '' }}">
                        {!! Form::label('donate_text', 'Текст в окне', ['class' => 'control-label col-sm-3']) !!}

                        <div class="col-sm-9">
                            {!! Form::textarea('donate_text', null, ['class' => 'form-control', 'rows' => 4]) !!}
                            <div class="help-block">{{ $errors->first('donate_text') }}</div>
                            <div class="help-block">Текст, который будет показываться в форме доната.</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>

                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop