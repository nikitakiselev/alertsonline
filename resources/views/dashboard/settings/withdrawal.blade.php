@extends('layouts.dashboard')

@section('title', 'Вывод средств - ' . config('alerts.site_name'))

@section('page-header')
    <h1><i class="fa fa-money"></i> Вывод средств</h1>
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
        <li class="active">Вывод средств</li>
    </ol>
@stop

@section('content')
    <div class="nav-tabs-custom">
        @include('dashboard.settings.tabs')

        <div class="tab-content">
            <div class="tab-pane active">
                <p>В настоящий момент доступен вывод средств только на кошелёк WebMoney. Это обусловлено желанием сделать минимальную потерю средств пользователя на комиссию платёжных систем.</p>
                <p>Процесс перевода ваших пожертвований на кошелёк WebMoney занимает от 30 минут до 3 рабочих дней.</p>

                {!! Form::model(Auth::user(), ['class' => 'form-horizontal']) !!}
                    <div class="form-group{{ $errors->has('withdraw_webmoney_purse') ? ' has-error' : '' }}">
                        {!! Form::label('withdraw_webmoney_purse', 'Номер кошелька Webmoney', ['class' => 'control-label col-md-3']) !!}

                        <div class="col-md-4">
                            {!! Form::text('withdraw_webmoney_purse', null, ['class' => 'form-control']) !!}
                            <div class="help-block">Например: R12345678901</div>
                            <div class="help-block">{{ $errors->first('withdraw_webmoney_purse') }}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i> Сохранить
                            </button>
                        </div>

                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop