<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" {!! Request::url() == route('dashboard.settings') ? 'class="active"' : '' !!}>
        <a href="{{ route('dashboard.settings') }}" role="tab">
            <i class="fa fa-user"></i> Аккаунт
        </a>
    </li>

    <li role="presentation" {!! Request::url() == route('dashboard.settings.donate') ? 'class="active"' : '' !!}>
        <a href="{{ route('dashboard.settings.donate') }}" role="tab">
            <i class="fa fa-credit-card"></i> Донат
        </a>
    </li>

    <li role="presentation" {!! Request::url() == route('dashboard.settings.withdrawal') ? 'class="active"' : '' !!}>
        <a href="{{ route('dashboard.settings.withdrawal') }}" role="tab">
            <i class="fa fa-money"></i> Вывод средств
        </a>
    </li>
</ul>