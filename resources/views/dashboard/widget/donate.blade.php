@extends('layouts.dashboard')

@section('title', 'Настройки виджета доната - ' . config('alerts.site_name'))

@section('page-header')
    <h1>Виджет доната <small>Настройки сообщения о донате</small></h1>
@stop

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Панель управления</a></li>
        <li>Виджеты</li>
        <li class="active">Виджет доната</li>
    </ol>
@stop

@section('content')
    <div class="donation-alert-link input-group">
        <input type="text" class="form-control" id="donation-alert-widget-link" value="{{ route('widget.donate_alertbox', Auth::user()->widget_token) }}" readonly="readonly" onclick="jQuery(this).select();"/>

        <span class="input-group-btn">
            <a href="{{ route('widget.donate_alertbox', Auth::user()->widget_token) }}" class="btn btn-default" target="_blank">Открыть</a>
        </span>

        <span class="input-group-btn">
            <button id="test-donate-alert" type="button" class="btn btn-default">Тест</button>
        </span>
    </div>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Настройки</h3>
        </div>

        <div class="box-body" style="display: block;">
            {!! Form::model(Auth::user(), ['class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group {{ $errors->has('donation_alert_min_amount') ? ' has-error' : '' }}">

                {!! Form::label('donation_alert_min_amount', 'Минимальная сумма (руб)', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-3">
                    <div class="input-group">
                        {!! Form::text('donation_alert_min_amount', null, ['class' => 'form-control']) !!}

                        <span class="input-group-btn">
                            <button class="btn btn-default" data-toggle="popover" title="Минимальная сумма" data-content="Минимальная сумма доната, при которой будет воспроизводиться сообщение голосом" data-placement="bottom" type="button">
                                <i class="fa fa-question"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div class="help-block">{{ $errors->first('donation_alert_min_amount') }}</div>
            </div>

            <div class="form-group{{ $errors->has('donation_alert_template') ? ' has-error' : '' }}">
                {!! Form::label('donation_alert_template', 'Шаблон сообщения', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-6">
                    <div class="input-group">
                        {!! Form::text('donation_alert_template', null, ['class' => 'form-control']) !!}

                        <span class="input-group-btn">
                            <button class="btn btn-default" data-toggle="popover" title="Шаблон сообщения" data-content="Шаблон сообщения, которое будет отображаться на экране. Поддерживаются следующие токены: </br><ul><li>{username} - имя донатера</li><li>{amount} - сумма доната</li></ul>" data-placement="bottom" type="button">
                                <i class="fa fa-question"></i>
                            </button>
                        </span>
                    </div>

                    <div class="help-block">{{ $errors->first('donation_alert_template') }}</div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('donation_alert_sound') ? ' has-error' : '' }}">
                {!! Form::label('donation_alert_sound', 'Звук нового уведомления', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-6">
                    <audio src="{{ asset(Auth::user()->settings->get('donation_alert_sound')) }}" controls></audio>
                    {!! Form::input('file', 'donation_alert_sound') !!}
                    <div class="help-block">{{ $errors->first('donation_alert_sound') }}</div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('donation_alert_animation') ? ' has-error' : '' }}">
                {!! Form::label('donate_bg', 'Анимация при уведомлении', ['class' => 'control-label col-sm-3']) !!}

                <div class="col-sm-9">
                    @if (Auth::user()->settings->get('donation_alert_animation'))
                        <img src="{{ asset(Auth::user()->settings->get('donation_alert_animation')) }}" alt="Donation animation" height="50"/>
                        <br><br>
                    @endif

                    {!! Form::input('file', 'donation_alert_animation') !!}
                    <div class="help-block">{{ $errors->first('donation_alert_animation') }}</div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop