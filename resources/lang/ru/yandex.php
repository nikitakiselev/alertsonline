<?php

return [

    'speech' => [

        'speakers' => [
            'zahar' => 'Захар',
            'ermil' => 'Омаж',
            'jane' => 'Джейн',
            'omazh' => 'Омаж'
        ],

        'emotions' => [
            'good' => 'Добрый',
            'neutral' => 'Нейтральный',
            'evil' => 'Злой',
            'mixed' => 'Смешанный'
        ],
    ],

];