# Real Alerts

```
cp .env.example .env
composer install
npm install
bower install
```

Config crontab

```
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```