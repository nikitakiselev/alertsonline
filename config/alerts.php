<?php

return [

    /**
     * Название сайта
     */
    'site_name' => 'Alerts Online',

    /**
     * Site email
     */
    'site_email' => 'mail@alertsonline.ru',

    /**
     * Комиссия сервиса в процентах
     */
    'commission' => 1,

];