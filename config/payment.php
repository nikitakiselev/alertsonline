<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Unitpay
    |--------------------------------------------------------------------------
    |
    | Ключи public_key b secret_key , берутся из настроек магазина
    | в панели управления Unitpay
    |
    */

    'unitpay' => [
        'public_key'  => env('UNITPAY_PUBLIC_KEY', null),
        'secret_key'  => env('UNITPAY_SECRET_KEY', null),
    ],

    /*
    |--------------------------------------------------------------------------
    | Webmoney
    |--------------------------------------------------------------------------
    |
    | Настройки Webmoney
    |
    */

    'webmoney' => [
        'purse' => env('WEBMONEY_PURSE', null),
        'wmid' => env('WEBMONEY_WMID', null),
        'key_file' => env('WEBMONEY_KEY_FILE', null),
        'key_password' => env('WEBMONEY_KEY_PASSWORD', null),
    ]

];