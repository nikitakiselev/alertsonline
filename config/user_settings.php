<?php

return [

    /**
     * Настройки пользователя по умолчанию
     */
    'defaults' => [
        'donate_minimum_amount' => 5, // Минимальная сумма доната
        'donate_text' => '',
        'donate_bg' => '',
        'account_avatar' => 'userdata/default/image/account_avatar.jpg',
        'donation_alert_min_amount' => '50',
        'donation_alert_sound' => 'userdata/default/audio/donation_alert_sound.wav',
        'donation_alert_template' => '{username} задонатил {amount} р.',
        'donation_alert_animation' => 'userdata/default/image/donation_alert_animation.gif',
        'withdraw_webmoney_purse' => '',
    ],

    /**
     * Размеры изображений
     */
    'image_styles' => [
        /**
         * Размеры фона
         */
        'donate_bg' => [
            'width' => 640,
            'height' => 192,
        ],

        /**
         * Размеры аватара
         */
        'account_avatar' => [
            'width' => 100,
            'height' => 100,
        ],
    ],
];