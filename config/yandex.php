<?php

return [

    // yandex dev key
    // https://developer.tech.yandex.ru/keys/
    'dev_key' => env('YANDEX_DEV_KEY', null),

    // speech options
    'speech' => [
        'speakers' => ['zahar', 'ermil', 'jane', 'omazh'],
        'emotions' => ['good', 'neutral', 'evil', 'mixed'],
    ],
];