<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'active' => $faker->boolean(),
    ];
});

$factory->define(App\Donation::class, function (Faker\Generator $faker) {

    $createdAt = $faker->dateTimeThisMonth();
    $amount = $faker->numberBetween(5, 25);
    $paid = $faker->boolean();
    $paidAt = $faker->dateTimeBetween($createdAt, \Carbon\Carbon::instance($createdAt)->addMinute(rand(5, 30)));

    return [
        'username' => $faker->name,
        'amount' => $amount,
        'message' => $faker->realText(50),
        'speaker' => $faker->randomElement(config('yandex.speech.speakers')),
        'emotion' => $faker->randomElement(config('yandex.speech.emotions')),
        'robot' => $faker->boolean(),
        'created_at' => $createdAt,
        'paid' => $paid,
        'paid_at' => $paid ? $paidAt : null,
    ];
});