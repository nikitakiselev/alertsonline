<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->integer('amount');
            $table->text('message');
            $table->string('speaker');
            $table->string('emotion');
            $table->boolean('robot')->default(false);
            $table->integer('user_id')->unsigned();
            $table->boolean('paid')->default(false);
            $table->decimal('paid_sum')->default(0);
            $table->timestamp('paid_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('donations');
    }
}
