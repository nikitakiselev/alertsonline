<?php

use App\Donation;
use App\User;
use Illuminate\Database\Seeder;

class DonationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Donation::truncate();

        $users = User::all();
        $donations = factory(Donation::class, 50)->make();

        $users->each(function($user) use ($donations) {
            $user->donations()->saveMany($donations);
        });
    }
}
