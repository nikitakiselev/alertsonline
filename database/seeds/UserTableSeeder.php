<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'kiselev2008@gmail.com';
        $admin->password = bcrypt('admin');
        $admin->active = true;
        $admin->save();
    }
}
