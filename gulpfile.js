var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.copy('resources/assets/images', 'public/images');
    mix.copy('resources/assets/fonts', 'public/fonts');

    mix.less('app.less');
    mix.less('dashboard.less', 'resources/assets/css');
    mix.less('donate.less');

    mix.styles([
        'resources/assets/css/adminlte.css',
        'resources/assets/css/dashboard.css'
    ], 'public/css/dashboard.css');

    mix.scripts([
        'bower_components/bootstrap/js/tooltip.js',
        'bower_components/bootstrap/js/popover.js',
        'js/app.js'
    ], 'public/js/app.js', 'resources/assets/');

    mix.scripts([
        'adminlte.js',
        'dashboard.js'
    ], 'public/js/dashboard.js');

    // donate alert box scripts
    mix.less('widgets/donation.less');
    mix.scripts([
        'widgets/donation.js'
    ], 'public/js/widgets/donation.js');

    mix.version([
        'css/app.css',
        'js/app.js',
        'js/widgets/donation.js',
        'css/donation.css',
        'public/css/dashboard.css',
        'public/js/dashboard.js'
    ]);
});
